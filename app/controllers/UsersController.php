<?php

use Amt\MoneyAdmin\Entities\User;

class UsersController extends BaseController
{
    public function get($id)
    {
        return User::where('id', '=', $id)->with('permission')->get();
    }
} 