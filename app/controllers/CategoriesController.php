<?php

use Amt\MoneyAdmin\Entities\Category;

class CategoriesController extends BaseController
{

    public function get()
    {
        $categories = Category::all();

        return ['error' => false, 'categories' => $categories->toArray()];
    }
} 