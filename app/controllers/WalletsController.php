<?php

use Amt\MoneyAdmin\Entities\Wallet;

/**
 * Class WalletsController
 */
class WalletsController extends BaseController
{
    public function index()
    {
        $wallets = Wallet::all();

        return View::make('wallets.index', compact('wallets'));
    }

    public function indexPartial()
    {
        $wallets = Wallet::all();

        return View::make('partials.wallets.index', compact('wallets'));
    }

    public function create()
    {
        return View::make('wallets.add');
    }

    public function store()
    {
        $wallet = Wallet::create(Input::all());

        if ($wallet->id > 0) {
            $result = ['error' => false];
        } else {
            $result = ['error' => true, 'errorMessage' => 'TODO'];
        }

        if (Request::ajax()) {
            return $result;
        } else {
            return Redirect::back('index');
        }
    }

    public function get($id)
    {
        $result = ['error' => false];

        try {
            $wallet = Wallet::find($id)->toArray();

            $result['data'] = $wallet;

        } catch(Exception $e) {
            $result = ['error' => true, 'errorMessage' => $e->getMessage()];
        }

        return $result;
    }
} 