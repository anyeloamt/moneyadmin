<?php


class SessionsController extends BaseController
{

    public function create()
    {
        if (Auth::check()) {
            return Redirect::route('wallets.index');
        }

        return View::make('sessions.create');
    }

    public function store()
    {
        $input = Input::all();

        $attempt = Auth::attempt([
            'username' => $input['username'],
            'password' => $input['password']
        ]);

        if ($attempt) return Redirect::intended('/');

        return Redirect::back()->with('response', 'Usuario o contraseña inválido/a');
    }

    public function destroy()
    {
        Auth::logout();

        return Redirect::route('sessions.create');
    }

}