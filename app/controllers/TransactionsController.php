<?php
use Amt\MoneyAdmin\Entities\Transaction;

use Intervention\Image\Facades\Image;

/**
 * Class TransaccionsController
 */
class TransactionsController extends BaseController
{

    public function byWallet($walletId)
    {
        $transactions = Transaction::where('wallet_id', '=', $walletId)
            ->with('wallet')
            ->with('files')
            ->get();

        foreach($transactions as & $transaction) {
            foreach($transaction->files as & $file) {
                $file->toViewModel();
            }
        }

        return View::make('transactions.list', compact('transactions'));
    }

    /**
     * @param int $wallet
     * @return \Illuminate\View\View
     */
    public function create($wallet)
    {
        return View::make('transactions.add');
    }

    public function store()
    {
        // Save transaction data
        $input = Input::except(['files', 'add_files']);

        /** @var Transaction $transaction */
        $transaction = Transaction::create($input);

        // Fire event of transaction creation
        Event::fire('transaction.created', [$transaction]);

        // If there's files, add transaction id and save them
        if (Input::hasFile('files')) {
            $this->saveFiles(Input::file('files'), $transaction);
        }
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile[] $files
     * @param Transaction $transaction
     */
    public function saveFiles(array $files, Transaction $transaction)
    {
        $filesDir = Config::get('app.files_dir');
        $filesMiniatureDir = Config::get('app.files_miniature_dir');

        foreach($files as $file) {

            if ($file->isValid()) {

                $filename = Str::random() . "." . $file->getClientOriginalExtension();

                $file = $file->move($filesDir, $filename);

                $file = \Amt\MoneyAdmin\Entities\File::create([
                    'path' => $filesDir . DIRECTORY_SEPARATOR . $filename,
                    'transaction_id' => $transaction->id
                ]);

                Image::make($file->path)
                    ->resize(150, 150)
                    ->save($filesMiniatureDir . DIRECTORY_SEPARATOR . $filename);
            }
        }
    }
} 