<?php

use Amt\MoneyAdmin\Observers\UserableEntityObserver;
use Amt\MoneyAdmin\Entities\Wallet;
use Amt\MoneyAdmin\Entities\Transaction;

Transaction::observe(new UserableEntityObserver);
Wallet::observe(new UserableEntityObserver);

Route::get('login', ['as' => 'sessions.create', 'uses' => 'SessionsController@create']);
Route::get('logout', ['as' => 'sessions.destroy', 'uses' => 'SessionsController@destroy']);
Route::post('login', ['as' => 'sessions.store', 'uses' => 'SessionsController@store']);

Route::group(['before' => ['app.login', 'auth']], function()
{
    Route::get('/', ['as' => 'wallets.index.home', 'uses' => 'WalletsController@index']);
    Route::get('/wallets', ['as' => 'wallets.index', 'uses' => 'WalletsController@index']);
    Route::get('/wallets/indexPartial', ['as' => 'wallets.indexPartial', 'uses' => 'WalletsController@indexPartial']);

    Route::get('/wallets/get', ['as' => 'wallets.get.noparams', 'uses' => 'WalletsController@get']);
    Route::get('/wallets/get/{wallet}', ['as' => 'wallets.get', 'uses' => 'WalletsController@get']);

    Route::get('/transactions/bywallet/{wallet}', ['as' => 'transactions.byWallet', 'uses' => 'TransactionsController@byWallet']);
    Route::get('/transactions/bywallet/', ['as' => 'transactions.byWallet.noparams', 'uses' => 'TransactionsController@byWallet']);

    Route::get('/categories/', ['as' => 'categories.get', 'uses' => 'CategoriesController@get']);
    Route::get('/users/{id}', ['as' => 'users.get', 'uses' => 'UsersController@get']);
    Route::get('/users/', ['as' => 'users.get.noparams', 'uses' => 'UsersController@get']);

    Route::group(['before' => ['permissions.add']], function()
    {
        Route::get('/wallets/create', ['as' => 'wallets.create', 'uses' => 'WalletsController@create']);

        Route::get('/transactions/create/{wallet}', ['as' => 'transactions.create', 'uses' => 'TransactionsController@create']);
        Route::get('/transactions/create', ['as' => 'transactions.create.noparams', 'uses' => 'TransactionsController@create']);

        Route::put('/transactions', ['as' => 'transactions.store', 'uses' => 'TransactionsController@store']);
        Route::put('/wallets', ['as' => 'wallets.store', 'uses' => 'WalletsController@store']);
    });
});

Event::listen('transaction.created', function(Transaction $transaction)
{
    /** @var Wallet $wallet */
    $wallet = Wallet::find($transaction->wallet_id);

    $wallet->updateAvailableBalance($transaction->amount);
});