@extends('sessions.master')

@section('content')

<div class="container">
    <div class="row">

        <div class="panel panel-danger col-xs-12 col-md-4 col-md-offset-4">
            <div class="panel-heading text-center"> Money Admin </div>
            <div class="panel-body">

                @if (Session::get('response'))
                    <div class="row">
                        <div class="col-md-4">
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Cerrar</span>
                                </button>
                                <strong>Mensaje del sistema</strong>
                                <div class="body">
                                    {{ Session::get('response') }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <h1 class="text-center">Iniciar sesi&oacute;n</h1>
                {{ Form::open(['url' => '/login', 'role' => 'form', 'method' => 'post', 'id' => 'login-form']) }}
                <div class="form-group">
                    {{ Form::label('username', 'Nombre de usuario:', array('class' => 'sr-only')) }}
                    {{ Form::text('username', null , array('class' => 'form-control','placeholder' => 'Nombre de usuario', 'data-bind' => 'value: username')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('username', 'Nombre de usuario:', array('class' => 'sr-only')) }}
                    {{ Form::password('password', array('class' => 'form-control','placeholder' => 'Contraseña', 'data-bind' => 'value: password')) }}
                </div>
                {{ Form::submit('Entrar', ['class' => 'btn btn-danger btn-lg btn-block', 'data-bind' => 'click: submit']) }}
                {{ Form::close() }}
                <hr>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
    {{-- HTML::style('css/login.css') --}}
@stop