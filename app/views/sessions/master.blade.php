<!DOCTYPE html>
<html>
<head>
    <title>Money Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    {{ HTML::style("/css/bootstrap-lumen.css") }}
    <style>
        @media only screen
        and (min-width : 1224px) {
            .top-padding {margin-top: 5%}
        }
        @media only screen
        and (min-device-width : 320px)
        and (max-device-width : 480px) {
            .top-padding {margin-top: 10%}
        }
    </style>
    @yield('styles')
</head>
<body class="top-padding">
<div class="container">
    <div class="row">
        @yield('content')
    </div>
</div>
{{ HTML::script('/js/jquery-1.11.1.min.js') }}
{{ HTML::script('/js/bootstrap.min.js') }}
@yield('scripts')

</body>
</html>