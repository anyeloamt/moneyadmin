<!DOCTYPE html>
<html>
<head>
    <title>Money Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    {{ HTML::style("/css/bootstrap-lumen.css") }}
    {{ HTML::style("/css/Styles.css") }}
    {{ HTML::style('/css/font-awesome.css') }}
    {{ HTML::style('/css/blueimp-gallery.min.css') }}
    @yield('styles')
</head>
<body class="top-padding">
<script type="text/template" id="messageTemplate">
    <div class="row">
        <div class="col-md-4">
            <div class="alert alert-{#type#} alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Cerrar</span>
                </button>
                <strong>Mensaje del sistema</strong>
                <div class="body">{#message#}</div>
            </div>
        </div>
    </div>
</script>
<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery"  data-use-bootstrap-modal="false">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<div class="loadingMask" style="display: none;">Cargando...</div>
<div class="container">
    <div class="row">
        @if (Auth::check())
            @include('partials.nav')
        @endif
    </div>

    <div class="row">
        <div id="messages"></div>
        <div id="userViewModelContainer"></div>
        @yield('content')
    </div>
</div>
<div class="modal fade" id="modalTemplate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center" id="modalTemplateTitle"></h4>
            </div>
            <div class="modal-body" id="modalTemplateBody">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

{{ HTML::script('/js/jquery-1.11.1.min.js') }}
{{ HTML::script('/js/bootstrap.min.js') }}
{{ HTML::script('/js/knockout-3.0.0.js') }}
{{ HTML::script('/js/knockout.mapping-latest.debug.js') }}
{{ HTML::script('/js/jquery.form.min.js') }}
{{ HTML::script('/js/blueimp-gallery.min.js') }}
{{ HTML::script('/js/linq.js') }}
{{ HTML::script('/js/moment.min.js') }}
{{ HTML::script('/js/App/Scripts.js') }}
@if (Auth::check())
<script type="text/javascript">
    $(function(){
        var userViewModel = {
            self: {{ Auth::user() }},
            permission: {{ Auth::user()->permission }}
        };

        userViewModel = ko.mapping.fromJS(userViewModel);
        ko.applyBindings(userViewModel, document.getElementById('userViewModelContainer'));
    });
</script>
@endif
@yield('scripts')

</body>
</html>