<div class="container">
    <h1>Cuentas</h1>
    @if ($wallets->count() <= 0)
    A&uacute;n no hay cuentas
    <a href="#" class="btn btn-danger" id="addWallet" title="Agregar nueva">
        <i class="fa fa-plus"></i>
    </a>
    @else
        @if (Auth::user()->permission->can_add)
            <div class="row">
                <div class="col-md-2 col-md-offset-10 col-xs-2 col-xs-offset-9">
                    <a href="#" class="btn btn-danger " id="addWallet" title="Agregar cuenta">
                        &nbsp;&nbsp;&nbsp;<i class="fa fa-plus hidden-md"></i>&nbsp;&nbsp;&nbsp;
                        <span class="hidden-xs">Agregar nueva</span>
                    </a>
                </div>
            </div>
        @endif
        <hr/>
        <div class="row">
            @foreach ($wallets as $wallet)
                <div class="col-md-4 font-big ">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h4 class="text-center lead white">
                                <strong>{{ $wallet->name }}</strong>
                            </h4>
                        </div>
                        <div class="panel-body text-center">
                            <p class="font-big ">
                                <strong>$ {{ number_format($wallet->available_balance, 2) }}</strong>
                            </p>
                            <p class="text-warning">Balance disponible</p>
                        </div>
                        <ul class="list-group list-group-flush text-center">
                            <li class="list-group-item">
                                {{ number_format($wallet->initial_balance, 2) }}
                                <p class="text-warning text-mini">Balance inicial</p>
                            </li>
                            <li class="list-group-item">
                                {{ $wallet->transactions->count() }}
                                <p class="text-warning text-mini">Total de transacciones</p>
                            </li>
                        </ul>
                        <div class="panel-footer">
                            <div class="row">
                                @if (Auth::user()->permission->can_add)
                                    <div class=" {{ $wallet->transactions->count() > 0 ? 'col-md-6 col-xs-6' : 'col-md-12 col-xs-12' }}" data-bind="visible: userViewModel.permissions().can_add">
                                        <a class="btn btn-lg btn-block btn-danger addTransaction" data-wallet="{{ $wallet->id }}" title="Agregar transacci&oacute;n" href="#">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                @endif
                                @if ($wallet->transactions->count() > 0)
                                <div class=" {{ Auth::user()->permission->can_add ? 'col-md-6 col-xs-6' : 'col-md-12 col-xs-12' }}">
                                    <a class="btn btn-lg btn-block btn-success viewTransactions" data-wallet="{{ $wallet->id }}" title="Ver transacciones" href="#">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>