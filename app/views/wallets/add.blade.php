<div class="top-padding"></div>
<div class="container-fluid font-big">
    <div class="row">
        <div class="panel panel-danger">
            <div class="panel-heading">Nueva cuenta</div>
            <div class="panel-body">
                <div class="form">
                    <fieldset>
                        {{ Form::open(array('url' => '/wallets', 'role' => 'form', 'method' => 'put')) }}
                        {{ Form::hidden('id', null, ['data-bind' => 'value: id']) }}
                        <div class="form-group">
                            {{ Form::label('name', 'Nombre:', array('class' => 'control-label sr-only')) }}
                            {{ Form::text('name', null , array('class' => 'form-control','placeholder' => 'Nombre', 'data-bind' => 'value: name')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('initial_balance', 'Balance inicial:', array('class' => 'control-label sr-only')) }}
                            {{ Form::input('number', 'initial_balance', null , array('class' => 'form-control', 'placeholder' => 'Balance inicial', 'data-bind' => 'value: initial_balance')) }}
                            {{ $errors->first('initial_balance', '<span class="errors">:message</span>') }}

                            {{ Form::hidden('available_balance', null, ['data-bind' => 'value: available_balance']) }}
                        </div>
                        <div class="form-group text-center">
                            {{ Form::submit('Guardar', array('class' => 'btn btn-primary', 'data-bind' => 'click: save')) }}
                            <a href="javascript:void()" class="btn btn-danger" data-bind="click: cancel">
                                Cancelar
                            </a>
                        </div>
                        {{ Form::close() }}
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>