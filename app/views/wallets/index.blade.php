@extends('partials.master')

@section('content')

<script type="text/javascript">
    var Urls = {
        Wallets: {
            create: "{{ route('wallets.create') }}",
            store: "{{ route('wallets.store') }}",
            indexPartial: "{{ route('wallets.indexPartial') }}",
            get: function(wallet) {
                return "{{ route('wallets.get.noparams') }}"
                    + ((wallet != undefined) ? "/" + wallet : "");
            }
        },
        Transactions: {
            create: function(wallet) {
                return "{{ route('transactions.create.noparams') }}"
                    + ((wallet != undefined) ? "/" + wallet : "");
            },
            byWallet: function(wallet) {
                return "{{ route('transactions.byWallet.noparams') }}"
                    + ((wallet != undefined) ? "/" + wallet : "");
            },
            store: "{{ route('transactions.store') }}"
        },
        Categories: {
            get: "{{ route('categories.get') }}"
        },
        Users: {
            get: function(user) {
                return "{{ route('users.get.noparams') }}"
                    + ((user != undefined) ? "/" + user : "");
            }
        }
    };
    var walletsContainer = 'walletsContainer';
    var transactionContainer = 'transactionContainer';
</script>
<div class="container-fluid">
    <div class="row">
        <div id="walletsContainer">
            @include('partials.wallets.index', compact('wallets'))
        </div>
    </div>
</div>
@stop

@section('scripts')
{{ HTML::script('js/App/Wallets/WalletViewModel.js') }}
{{ HTML::script('js/App/Categories/CategoryViewModel.js') }}
{{ HTML::script('js/App/Transactions/TransactionViewModel.js') }}
{{ HTML::script('js/App/Wallets/Index.js') }}
@stop