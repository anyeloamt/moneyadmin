<style>
    table {
        border: 1px solid black;
        border-collapse: collapse;
        width: 700px;
        font-size: 122%;
    }
    tbody {
        /*font-size: 15px;*/
        font-weight: normal
    }
    td.hidden {
        border-width: 0px;
        padding: 0px;
    }
    td.hiddenTop {
        border-top-width: 0px;
    }
    table th {background-color: #f5f5f5}
    li.list-group-item {border-bottom: none;}

    .thumbs div a img {
        width: 10%;
        margin-left: 1%;
        margin-right: 1%;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <h1>Vista de transacciones</h1>
        </div>
        <div class="col-md-5 col-xs-6">
            <div class="pull-right" style="padding-top: 20px">
                <a href="#" class="btn btn-danger" id="back" title="Regresar">
                    &nbsp;&nbsp;&nbsp;<i class="fa fa-backward hidden-md"></i>&nbsp;&nbsp;&nbsp;
                    <span class="hidden-xs">regresar</span>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="panel panel-danger">
            <div class="panel-heading font-big">Transacciones de "<span data-bind="text: wallet().name"></span>"</div>
            <div class="agenda">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Categor&iacute;a</th>
                            <th class="hidden-xs">Nota</th>
                            <th>Cantidad</th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: byDate">
                            <tr>
                                <td class="agenda-date active" data-bind="attr: { rowspan: rowSpan }">
                                    <div class="dayofmonth" data-bind="text: moment(created_at()).format('D')"></div>
                                    <div class="text-center">
                                        <div class="dayofweek" data-bind="text: moment(created_at()).format('dddd')"></div>
                                        <div class="shortdate" data-bind="text: moment(created_at()).format('MMMM, YYYY')"></div>
                                    </div>
                                </td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                            <!-- ko foreach: transactions -->
                            <tr>
                                <td class="hiddenTop font-big">
					<span class="label label-default" data-bind="text: $root.category($data.category_id())"></span>
				</td>
                                <td class="hiddenTop hidden-xs" data-bind="text: $data.note"></td>
                                <td class="hiddenTop text-right"">
                                    <a href="#" class="btn-link" data-bind="text: $data.amount.formatMoney(), click: $root.showTransactionModal"></a>
                                </td>
                            </tr>
                            <!-- /ko -->
                            <tr>
                                <td colspan="2" class="hidden-xs bold">Cant. total del d&iacute;a</td>
                                <td colspan="1" class="hidden-md hidden-lg bold">Cant. total del d&iacute;a</td>
                                <td data-bind="text: totalAmount.formatMoney()" class="bold text-right"></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3" class="hidden-xs bold">Cant. total</td>
                                <td colspan="2" class="hidden-md hidden-lg bold">Cant. total</td>
                                <td data-bind="text: totalAmount.formatMoney()" class="bold text-right"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <pre data-bind="text: ko.toJSON(wallet().name, null, 2)"></pre> --}}
<div style="display: none" id="modalTransactionTemplate">
    <div class="row font-big">
        <div class="panel panel-danger">
            <ul class="list-group list-group-flush text-center">
                <li class="list-group-item" data-bind="text: moment(selectedTransaction().created_at()).format('dddd D [de] MMMM [del] YYYY [a las] H:mm:s')"></li>
                <p class="text-warning text-mini">Fecha</p>
                <li class="list-group-item font-big">
    		    <span class="label label-default" data-bind="text: $root.category(selectedTransaction().category_id())"></span>
		</li>
                <p class="text-warning text-mini">Categor&iacute;a</p>
                <li class="list-group-item" data-bind="text: selectedTransaction().note()"></li>
                <p class="text-warning text-mini">Nota</p>
                <li class="list-group-item" data-bind="text: selectedTransaction().amount.formatMoney()"></li>
                <p class="text-warning text-mini">Cantidad</p>
                <li class="list-group-item thumbs">
                    <span data-bind="visible: selectedTransaction().files().length <= 0">Sin fotos</span>
                    <div id="imageLinks" class="row" data-bind="foreach: selectedTransaction().files()">
                        <a data-bind="attr: { href: path, title: $parent.selectedTransaction().note() }" data-gallery="#blueimp-gallery-imageLinks" data-filter="" class="">
                            <img data-bind="attr: { src: miniature_path, alt: $parent.selectedTransaction().note() }"/>
                        </a>
                    </div>
                </li>
                <p class="text-warning text-mini">Fotos</p>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
    var transactionsJson = {{ $transactions }};
    document.getElementById('imageLinks').onclick = function (event) {
    // function linkxx (event) {
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event},
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
        console.log(links, options, target);
    };
</script>
