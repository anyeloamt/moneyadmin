<div class="top-padding"></div>
<div id="transactionContainer">
    <div class="container-fluid font-big">
        <div class="row">
            <div class="panel panel-danger">
                <div class="panel-heading">Nueva transacci&oacute;n</div>
                <div class="panel-body">
                    <div class="form">
                        <fieldset>
                            {{ Form::open(['route' => 'transactions.store', 'role' => 'form', 'method' => 'put', 'enctype' => 'multipart/form-data']) }}
                            {{ Form::hidden('id', null, ['data-bind' => 'value: id']) }}
                            {{ Form::hidden('wallet_id', null, ['data-bind' => 'value: wallet_id']) }}

                            <div class="form-group">
                                {{ Form::label('note', 'Nota:', array('class' => 'control-label sr-only')) }}
                                {{ Form::textarea('note', null , array('class' => 'form-control','placeholder' => 'Nota del gasto', 'data-bind' => 'value: note')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('initial_balance', 'Categor&iacute;a:', array('class' => 'control-label sr-only')) }}
                                {{ Form::select('category_id', [] ,null, array('class' => 'form-control', 'data-bind' => "options: categoriesVm, optionsText: 'name', optionsValue: 'id', value: category_id, optionsCaption: '--Selecciona--'")) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('amount', 'cantidad:', array('class' => 'control-label sr-only')) }}
                                {{ Form::input('number', 'amount', null , array('class' => 'form-control', 'placeholder' => 'Cantidad', 'data-bind' => 'value: amount')) }}
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    {{ Form::input('checkbox', 'add_files', null, ['data-bind' => 'checked: add_files']) }}
                                    <span class="text-label-checkbox">Agregar archivos</span>
                                </label>
                                <div data-bind="fadeVisible: add_files">
                                    {{ Form::input('file', 'files[]', null , ['class' => 'form-control', 'placeholder' => 'Fotos', 'multiple' => 'multiple']) }}
                                </div>
                            </div>
                            <div class="form-group text-center">
                                {{ Form::submit('Guardar', array('class' => 'btn btn-primary', 'data-bind' => 'click: save')) }}
                                <a href="javascript:void()" class="btn btn-danger" data-bind="click: cancel">
                                    Cancelar
                                </a>
                            </div>
                            {{ Form::close() }}
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>