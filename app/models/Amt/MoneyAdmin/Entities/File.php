<?php

namespace Amt\MoneyAdmin\Entities;

class File extends EntityAbstractModel
{
    public $table = 'files';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction()
    {
        return $this->belongsTo('Amt\MoneyAdmin\Entities\Transaction', 'transaction_id');
    }

    public function toViewModel()
    {
        $ds = DIRECTORY_SEPARATOR;

        $this->path = "/" . substr($this->path, strpos($this->path, 'files'));

        // For windows directory separator web path.
        $this->path = str_replace($ds, '/', $this->path);
        $this->miniature_path = str_replace('files' . $ds, 'files' . $ds . 'miniature' .$ds, $this->path);
    }
}