<?php

namespace Amt\MoneyAdmin\Entities;

/**
 * Class Category
 * @package Amt\Entities
 */
class Category extends EntityAbstractModel
{
    protected $table = 'categories';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany('Amt\Entities\Transaction', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('Amt\Entities\Type', 'type_id');
    }
} 