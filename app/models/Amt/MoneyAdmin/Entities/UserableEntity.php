<?php

namespace Amt\MoneyAdmin\Entities;

/**
 * Class UserableEntity
 * @property int creator_id
 * @property int modificator_id
 * @package Amt\Entities
 */
class UserableEntity extends EntityAbstractModel
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('Amt\MoneyAdmin\Entities\User', 'creator_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function modificator()
    {
        return $this->belongsTo('Amt\MoneyAdmin\Entities\User', 'modificator_id');
    }
} 