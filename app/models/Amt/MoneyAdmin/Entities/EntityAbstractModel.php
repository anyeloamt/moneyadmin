<?php

namespace Amt\MoneyAdmin\Entities;

use Eloquent;

/**
 * Class EntityAbstractModel
 * @package Amt\Entities
 *
 * @author Anyelo Almánzar Mercedes <anyeloamt@gmail.com>
 */
class EntityAbstractModel extends Eloquent
{
    /**
     * Guarded fields
     *
     * @var array
     */
    protected $guarded = ['id'];
} 