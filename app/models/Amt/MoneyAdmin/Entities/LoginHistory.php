<?php

namespace Amt\MoneyAdmin\Entities;

/**
 * Class LoginHistory
 * @package Amt\MoneyAdmin\Entities
 */
class LoginHistory extends EntityAbstractModel
{
    /**
     * @var string
     */
    protected $table = 'login_history';

    /**
     * @var bool
     */
    public $timestamps = false;
} 