<?php

namespace Amt\MoneyAdmin\Entities;
/**
 * Class Wallet
 * @property int $id
 * @property string $name
 * @property double $initial_balance
 * @property double $available_balance
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Wallet extends UserableEntity
{
    protected $table = 'wallets';

    protected $fillable = [
        'name',
        'initial_balance',
        'available_balance'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany('Amt\MoneyAdmin\Entities\Transaction', 'wallet_id');
    }

    /**
     * @param double $amount
     */
    public function updateAvailableBalance($amount)
    {
        $this->available_balance -= $amount;

        $this->save();
    }

    /**
     * Overrides newQuery method to add custom conditions
     *
     * @param bool $excludeDeleted
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newQuery($excludeDeleted = true)
    {
        $query = parent::newQuery($excludeDeleted);

        $query->where('active', '=', true);

        return $query;
    }
}