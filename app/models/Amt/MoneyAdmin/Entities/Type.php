<?php
namespace Amt\MoneyAdmin\Entities;

/**
 * Class Type
 * @package Amt\Entities
 */
class Type extends EntityAbstractModel
{
    protected $table = 'types';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany('Amt\Entities\Category', 'type_id');
    }
} 