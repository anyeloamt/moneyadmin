<?php

namespace Amt\MoneyAdmin\Entities;

/**
 * Class Transaction
 * @package Amt\Entities
 * @property integer $category_id
 * @property string $note
 * @property double $amount
 * @property integer $wallet_id
 */
class Transaction extends UserableEntity
{
    protected $table = 'transactions';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo('Amt\MoneyAdmin\Entities\Wallet', 'wallet_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('Amt\Entities\MoneyAdmin\Category', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany('Amt\MoneyAdmin\Entities\File', 'transaction_id');
    }
} 