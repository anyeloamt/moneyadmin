<?php

namespace Amt\MoneyAdmin\Entities;

class Permission extends EntityAbstractModel
{
    protected $table = 'permissions';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('Amt\MoneyAdmin\Entities\User', 'permissions_id');
    }
} 