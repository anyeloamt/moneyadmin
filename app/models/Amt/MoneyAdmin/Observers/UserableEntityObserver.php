<?php

namespace Amt\MoneyAdmin\Observers;

use Amt\MoneyAdmin\Entities\UserableEntity;

/**
 * Class WalletObserver
 * @package Amt\MoneyAdmin\Observers
 */
class UserableEntityObserver
{
    /**
     * @param UserableEntity $entity
     */
    public function creating(UserableEntity $entity)
    {
        $entity->creator_id = \Auth::user()->id;
    }

    /**
     * @param UserableEntity $entity
     */
    public function saving(UserableEntity $entity)
    {
        $entity->modificator_id = \Auth::user()->id;
    }
} 