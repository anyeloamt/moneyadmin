<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserReferenceToAllTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('wallets', function(Blueprint $t)
        {
            $t->unsignedInteger("creator_id")->nullable()->index();
            $t->unsignedInteger("modificator_id")->nullable()->index();

            $t->foreign("creator_id")->references('id')->on('users');
            $t->foreign("modificator_id")->references('id')->on('users');
        });

        Schema::table('transactions', function(Blueprint $t)
        {
            $t->unsignedInteger("creator_id")->nullable()->index();
            $t->unsignedInteger("modificator_id")->nullable()->index();

            $t->foreign("creator_id")->references('id')->on('users');
            $t->foreign("modificator_id")->references('id')->on('users');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('wallets', function(Blueprint $t)
        {
            $t->dropForeign("wallets_creator_id_foreign");
            $t->dropForeign("wallets_modificator_id_foreign");

            $t->dropColumn("creator_id");
            $t->dropColumn("modificator_id");
        });

        Schema::table('transactions', function(Blueprint $t)
        {
            $t->dropForeign("transactions_creator_id_foreign");
            $t->dropForeign("transactions_modificator_id_foreign");

            $t->dropColumn("creator_id");
            $t->dropColumn("modificator_id");
        });
	}

}
