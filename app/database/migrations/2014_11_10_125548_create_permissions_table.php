<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permissions', function(Blueprint $table)
		{
			$table->increments('id');
            $table->boolean('can_add');
            $table->boolean('can_select');
            $table->boolean('can_update');
            $table->boolean('can_delete');
			$table->timestamps();
		});

        Schema::table('users', function(Blueprint $table)
        {
            $table->unsignedInteger('permissions_id')->nullable();
            $table->foreign('permissions_id')->references('id')->on('permissions')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropForeign("users_permissions_id_foreign");
            $table->dropColumn('permissions_id');
        });

		Schema::drop('permissions');
	}

}
