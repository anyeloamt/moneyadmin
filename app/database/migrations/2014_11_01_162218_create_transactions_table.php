<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table)
		{
			$table->increments('id');

            $table->unsignedInteger("category_id")->index();
            $table->unsignedInteger("wallet_id")->index();

            $table->string("note");
            $table->double("amount");

            $table->foreign("category_id")->references('id')->on('categories')->onDelete('cascade');
            $table->foreign("wallet_id")->references('id')->on('wallets')->onDelete('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
