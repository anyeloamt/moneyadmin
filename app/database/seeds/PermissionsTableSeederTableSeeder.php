<?php

use Amt\MoneyAdmin\Entities\Permission;
use Amt\MoneyAdmin\Entities\User;

class PermissionsTableSeederTableSeeder extends Seeder
{

	public function run()
	{
        Permission::create([
            'can_select' => true,
            'can_add' => true,
            'can_update' => true,
            'can_delete' => true,
        ]);

        Permission::create([
            'can_select' => true,
            'can_add' => false,
            'can_update' => false,
            'can_delete' => false,
        ]);

        $anyelo = User::find(1);
        $nena = User::find(2);

        $anyelo->permissions_id = 1;
        $nena->permissions_id = 2;

        $anyelo->save();
        $nena->save();
	}

}