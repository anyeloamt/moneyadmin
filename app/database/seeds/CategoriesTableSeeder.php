<?php
use Amt\MoneyAdmin\Entities\Category;
use Amt\MoneyAdmin\Entities\Type;

/**
 * Class CategoriesTableSeeder
 */
class CategoriesTableSeeder extends Seeder {

	public function run()
	{
        Type::create(['name' => 'Gasto']);
        Type::create(['name' => 'Ingreso']);

        Category::create([
            'name' => 'Comida',
            'id_type' => '1'
        ]);

        Category::create([
            'name' => 'Romo',
            'id_type' => '1'
        ]);

        Category::create([
            'name' => 'Pasaje',
            'id_type' => '1'
        ]);

        Category::create([
            'name' => 'Préstamo',
            'id_type' => '1'
        ]);
	}

}