<?php

use Amt\MoneyAdmin\Entities\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        // User::truncate();

        User::create([
            'username' => 'anyeloamt',
            'email' => 'anyeloamt@gmail.com',
            'first_name' => 'Anyelo',
            'last_name' => 'Almánzar Mercedes',
            'password' => Hash::make('mueran'),
        ]);

        User::create([
            'username' => 'manuelacastillo',
            'email' => 'djjose1982@gmail.com',
            'first_name' => 'Manuela',
            'last_name' => 'Castillo Ortega',
            'password' => Hash::make('holaangel'),
        ]);
    }
} 