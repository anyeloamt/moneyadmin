/*global $, document, window, Enumerable, console */

$.ajaxSetup({
    success: function(response) {
        if (typeof response == 'string') {
            if (response.indexOf('notLoggedIn') != -1 || response.indexOf('access_denied') != -1) {
                try {
                    response = JSON.parse(response);
                } catch (er) {
                    console.log("Se produjo un error ", er);
                }
            }
        }
        if (response.notLoggedIn) {
            alert("No has iniciado sessión ó has tardado mucho tiempo fuera, por favor inicia sesión para continuar.");
            location.reload();
        }
        if (response.access_denied) {
            alert(response.errorMessage);
            location.reload();
        }
    }
});
$(document).ajaxStart(function(){
    toggleLoadingMask();
});
$(document).ajaxStop(function(){
    toggleLoadingMask();
    $(document).find('[title]').tooltip();
});
$(document).ajaxError(function(){
    alert("Se produjo un error en el request, por favor recarga la página");
    console.log("Se produjo un error en el request", arguments);
});

$(document).find('[title]').tooltip();

function loadPartial(url, divID, onCompleted) {
    $("#" + divID).load(url, onCompleted);
}

function addMessage(message, type) {
    var messageTemplate = $('#messageTemplate').html().replace('{#message#}', message)
        .replace('{#type#}', type || 'danger');
    $('#messages').append(messageTemplate);
}

function showModal(body, title) {
    if (title) $('#modalTemplateTitle').text(title);
    $('#modalTemplateBody').html(body);
    $('#modalTemplate').modal();
}

function toggleLoadingMask(){
    $('.loadingMask').fadeToggle();
}

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

Date.prototype.formatNormal = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = this.getDate().toString();
    return yyyy + "/" + (mm[1]?mm:"0"+mm[0])  + "/" + (dd[1]?dd:"0"+dd[0]); // padding
};

if ( !Array.prototype.forEach ) {
    Array.prototype.forEach = function(fn, scope) {
        for(var i = 0, len = this.length; i < len; ++i) {
            fn.call(scope, this[i], i, this);
        }
    }
}

$(function(){
    ko.bindingHandlers.fadeVisible = {
        init: function(element, valueAccessor) {
            $(element).toggle(ko.utils.unwrapObservable(valueAccessor()));
        },
        update: function(element, valueAccessor) {
            ko.utils.unwrapObservable(valueAccessor()) ? $(element).slideDown() : $(element).slideUp();
        }
    };

    ko.subscribable.fn.formatMoney = function(c, d, t) {
        var number = Number(this());
        if (!isNaN(number)) {
            return number.formatMoney(c, d, t);
        } else {
            throw new Error("Just number subscribable can formatMoney");
        }
    };

    moment.locale('es');
});