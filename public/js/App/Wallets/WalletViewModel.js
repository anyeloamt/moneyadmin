/*global $, walletsContainer, document, window, ko, console */
var walletVm;
var walletVmContainer = document.getElementById(walletsContainer);

var WalletViewModel = function() {
    var self = this;

    self.id = ko.observable();
    self.name = ko.observable();
    self.initial_balance = ko.observable().extend({numeric: 2});
    self.available_balance = ko.observable().extend({numeric: 2});
    self.created_at = ko.observable();
    self.updated_at = ko.observable();

    self.propName = ko.computed(function(){
        return self.name();
    });

    self.transactions = ko.observableArray();

    self.validate = function(model) {
        var result = true;
        if (model.initial_balance() == undefined
            || model.initial_balance() == null
            || model.initial_balance() <= 0) {
            result = false;
            addMessage("Agrega el nombre de la cuenta");
        }

        if (model.name() == undefined || model.name() == null || model.name().trim() == "") {
            result = false;
            addMessage("Agrega el balance inicial de la cuenta");
        }

        return result;
    };

    self.isNew = function() {
        return self.id() == undefined || self.id() <= 0;
    }

    self.beforeSave = function(model) {
        if (self.isNew()) {
            self.available_balance(self.initial_balance());
        }
    };

    self.save = function(model) {
        self.beforeSave(model);

        if (self.validate(model)) {
            var serializedForm = $('#' + walletsContainer)
                .find('form').first().serialize();
            $.ajax({
                url: Urls.Wallets.store,
                method: 'put',
                data: serializedForm
            }).done(function(data) {
                if (data.error)  {
                    addMessage(data.errorMessage);
                } else {
                    loadPartial(Urls.Wallets.indexPartial, walletsContainer);
                }
            });
        }
    };

    self.cancel = function(model) {
        if (confirm("¿Cancelar?")) {
            loadPartial(Urls.Wallets.indexPartial, walletsContainer);
        }
    };
};

function initWallets(walletsJson) {
    walletVm = new WalletViewModel();
    ko.cleanNode(walletVmContainer);
    ko.mapping.fromJS(walletsJson, null, walletVm);
    ko.applyBindings(walletVm, walletVmContainer);
}