/*global $*/
var transactionListVm;

$('body').on('click', '#addWallet', function(){
    loadPartial(Urls.Wallets.create, walletsContainer, function(){
        initWallets({});
    });
});

$('body').on('click', '.viewTransactions', function(){
    loadPartial(Urls.Transactions.byWallet($(this).data('wallet')), walletsContainer, function(){
        transactionListVm = initTransactionList(transactionsJson);
        var transactionListContainer = document.getElementById(walletsContainer);
        ko.cleanNode(transactionListContainer);
        ko.applyBindings(transactionListVm, transactionListContainer);

        $('#back').click(function(){
            loadPartial(Urls.Wallets.indexPartial, walletsContainer);
        });
    });
});

$('body').on('click', '.addTransaction', function(){
    var walletId = $(this).data('wallet');
    loadPartial(Urls.Transactions.create(walletId), walletsContainer, function(){
        // 1. Load wallet by id
        $.get(Urls.Wallets.get(walletId))
            .done(function(response) {
                if (response.error) {
                    // TODO
                    console.log('Error buscando el wallet', response);
                } else {
                    initTransactionVm({wallet: ko.mapping.fromJS(response.data)});
                }
            });
    });
});