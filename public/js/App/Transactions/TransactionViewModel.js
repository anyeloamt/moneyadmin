/*global $, transactionContainer, document, window, ko, console, moment */
var transactionVm;
var transactionListContainer = document.getElementById(transactionContainer);
var categoriesVm = ko.observableArray();

var TransactionListViewModel = function(wallet, transactions){
    var self = this;
    this.wallet = ko.observable(wallet);
    this.transactions = ko.observableArray(transactions);
    this.selectedTransaction = ko.observable(transactions[0]);

    this.byDate = ko.computed(function(){
        var grouped;

        grouped = Enumerable.From(self.transactions())
            .GroupBy(function (transaction) { return moment(transaction.created_at()).format('YYYY/MM/DD') },
            function (transaction) {
                return transaction;
            },
            function (created_at, grouping) {
                return {
                    created_at: ko.observable(moment(created_at).format('YYYY/MM/DD')),
                    transactions: grouping.source,
                    rowSpan: (grouping.source.length + 2),
                    totalAmount: Enumerable.From(grouping.source).Sum(function(t){
                        return Number(t.amount())
                    })
                }
            }).ToArray();

        return grouped;
    });

    this.totalAmount = Enumerable.From(this.transactions()).Sum(function(t){
        return Number(t.amount());
    });

    this.showTransactionModal = function(model) {
        self.selectedTransaction(model);
        showModal($('#modalTransactionTemplate').html(), "Detalles de la transacción");
    };

    this.category = function(model) {
        var result = '';
        categoriesVm().forEach(function(c) {
            if (c.id() == model) {
                result = c.name();
            }
        });

        return result;
    }
};

var TransactionViewModel = function() {
    var self = this;

    // Load categories.
    if (categoriesVm().length == 0) {
        categoriesVm = ko.observableArray([]);
        $.ajax({
            url: Urls.Categories.get,
            dataType: 'json',
            async: false
        }).done(function(data){
                if (!data.error) {
                    data.categories.forEach(function(element){
                        var categoryVm = ko.mapping.fromJS(element, null, new CategoryViewModel());
                        categoriesVm.push(categoryVm);
                    });
                } else {
                    console.log("Error ", data);
                }
            });
    }

    self.id = ko.observable();
    self.note = ko.observable();
    self.category_id = ko.observable();
    self.wallet_id = ko.observable();
    self.amount = ko.observable();

    self.created_at = ko.observable();
    self.updated_at = ko.observable();

    self.wallet = ko.observable();

    // Form-only properties
    self.add_files = ko.observable(false);

    self.validate = function(model) {
        var result = true;
        if (model.note() == undefined
            || model.note() == null
            || model.note() <= 0) {
            result = false;
            addMessage("Agrega una nota");
        }

        if (model.category_id() == undefined || model.category_id() == null || model.category_id() <= 0) {
            result = false;
            addMessage("Agrega la categoría");
        }

        if (model.wallet_id() == undefined || model.wallet_id() == null || model.wallet_id() <= 0) {
            result = false;
            addMessage("No hay una cuenta seleccionada.");
        }

        if (model.amount() == undefined || model.amount() == null || model.amount() <= 0) {
            result = false;
            addMessage("Agrega la cantidad");
        }

        return result;
    };

    self.isNew = function() {
        return self.id() == undefined || self.id() <= 0;
    }

    self.beforeSave = function(model) {
        if (self.wallet() != undefined) {
            self.wallet_id(self.wallet().id());
        }
    };

    self.save = function(model) {
        self.beforeSave(model);

        if (self.validate(model)) {
            var form = $('#' + walletsContainer).find('form').first();
            form.ajaxForm({
                url: Urls.Transactions.store,
                success:function(data) {
                    if (data.error)  {
                        addMessage(data.errorMessage);
                    } else {
                        loadPartial(Urls.Wallets.indexPartial, walletsContainer);
                    }
                }
            }).submit();
        }
    };

    self.cancel = function(model) {
        if (confirm("¿Cancelar?")) {
            loadPartial(Urls.Wallets.indexPartial, walletsContainer);
        }
    };
};

function initTransactionVm(transactionJson) {
    transactionVm = new TransactionViewModel();
    transactionListContainer = document.getElementById(transactionContainer);
    ko.cleanNode(transactionListContainer);
    ko.mapping.fromJS(transactionJson, null, transactionVm);
    ko.applyBindings(transactionVm, transactionListContainer);

    transactionVm.wallet.subscribe(function(value){
        if (value != null) {
            transactionVm.wallet_id(value.id());
        }
    });
}

function initTransactionList(transactions) {
    var grouped = Enumerable.From(transactions)
        .GroupBy(function (transaction) { return transaction.wallet_id },
        function (transaction) {
            return transaction;
        },
        function (wallet, grouping) {
            return {
                wallet: wallet,
                transactions: grouping.source
            }
        }).ToArray()[0];

    grouped.transactions = Enumerable.From(grouped.transactions).Select(function(t){
        return ko.mapping.fromJS(t, null, new TransactionViewModel());
    }).ToArray();

    return new TransactionListViewModel(ko.mapping.toJS(grouped.transactions[0].wallet()), grouped.transactions);
}