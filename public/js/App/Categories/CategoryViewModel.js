
/*global ko */

var CategoryViewModel = function() {
    var self = this;

    self.id = ko.observable();
    self.id_type = ko.observable();
    self.name = ko.observable();
};