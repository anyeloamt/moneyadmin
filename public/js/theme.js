$(document).ready(function() {

	/*
	 * ================================= Sidebar tabs
	 * =================================
	 */
	$('.sidebar-icon a:not(:first)').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
	});

	/*
	 * ================================= Show menu button on mobile
	 * =================================
	 */
	$("#show-menu").click(function(e) {
		e.preventDefault();
		$("#wrapper").toggleClass("active");
	});

	/*
	 * ================================= Custom file dialog
	 * =================================
	 */
	$('.show-file-dialog').click(function(e) {
		e.preventDefault();
		var targetDialog = $(this).attr('href');
		$(targetDialog).click();
	});

	/*
	 * ================================= Equalize columns
	 * =================================
	 */
	var maxHeight = 0;

	$(".activity-feed-wrapper").each(function() {
		if ($(this).height() > maxHeight) {
			maxHeight = $(this).height();
		}
	});

	$(".activity-feed-wrapper").height(maxHeight);

	/* End scripts */
	var transactionId = null;
	$('[data-btn="remove"]').click(function(e) {
		e.preventDefault();
		var h = $(this);
		var g = h.closest('.item-transactions');
		var userid = h.data('userid');
		transactionId = g.data('transaction');
		$('#confirmForm').attr('data-userid', userid).modal();
		document.onkeydown = checkKeycode;
	});
	$('[data-btn="submit"]').click(function(e) {
		e.preventDefault();
		var modal = $('#confirmForm');
		var userid = modal.data('userid');
		$.get('/ajax/transaction/' + transactionId + '/delete', {account_id: userid},function(data) {
			$('[data-transaction="' + transactionId + '"]').slideUp();
			modal.modal('hide');
		});
	});
	function checkKeycode(e) {
		var keycode;
		if (window.event)
			keycode = window.event.keyCode;
		else if (e)
			keycode = e.which;
		if (keycode === 13)
			$('#confirmForm').modal('hide');
	};

	var activeForm = true;
	var mainForm = null;
	var mainModal = '#myModal';
	var addMoreActive = false;
	String.prototype.testRegex = function(regex){
		var strText = this;
		var i = 0;
		var len = strText.length;
		while (i < len){
			var new_text = strText.substr(i,1);
			if(!regex.test(new_text)) {
				return false;
			}
			i++;
		}
		return true;
	};
	var regex = /[0-9.]/;
	var validateMLValue = function(inp, value){
		if(value.length > 0){
			if(!value.testRegex(regex)){
				value = value.substr(0, value.length-1);
			} else {
				value = value.replace('..', '.');
			}
			inp.val(replaceFirst(value));
		}
	};
	var replaceFirst = function(value){
		if(value.indexOf('.') === 0){
			value = '0' + value;
		}
		return value;
	};
	var replaceLast = function(value){
		if(value.length == value.lastIndexOf('.') + 1){
			value = value.substr(0, value.length-1);
		};
		return value;
	};
	var getCurrentDay = function(format){
		return moment().format(format);
	};
	$(document).mousedown(function(){
		if(!activeForm) return false;
	});

	$('#myModal').on('show.bs.modal', function (){
		getCategory($(this), function(){
			$('[href="#expenseTab"]').click();
		});
	});
	$('#myModal').on('hidden.bs.modal', function(){
		if(addMoreActive){
			window.location.reload();
		}
	});
	
	$('[href="#myModal"]').click(function(){
		mainForm = $('#myModal').html();
	});

	$(mainModal).on('keyup', '[name="ML_value"]', function(e){
		var inpValue = $(this).val();
		validateMLValue($(this), inpValue);
	});
	$(mainModal).on('keydown', '[name="ML_value"]', function(e){
		var inpValue = $(this).val();
		validateMLValue($(this), inpValue);
	});
	$(mainModal).on('blur', '[name="ML_value"]', function(e){
		$(this).val(replaceLast($(this).val()));
	});
	$(mainModal).on('focus', '[name="ML_date"]', function(e){
		e.preventDefault();
		$('#datepicker').datepicker('show');
		if($(this).val() == ''){
			var dateFormat = $(this).data('moment-format');
			$(this).val(getCurrentDay(dateFormat));
		}
	});
	$(document).on('click', '.nav-lstItem li', function(){
		$('.nav-lstItem li').removeClass('active');
		var h = $(this);
		var modal = h.closest('.modal');
		var MLtype = h.closest('.mltab-type').data('type');
		var MLtypeValue = h.data('value');
		$('[name="ML_type"]', modal).val(MLtype);
		$('[name="ML_type_value"]', modal).val(MLtypeValue);
		h.addClass('active');
	});
	$(document).on('click', '[data-toggle="tab"]', function(e){
		e.preventDefault();
		var h = $(this);
		var container = h.closest('.module-content');
		var tab = h.attr('href');
		console.log(tab);
		$('.mltab-type', container).removeClass('active in');
		$(tab, container).addClass('active in');
	});
	$(document).on('click', '.usergroup li', function(e){
		e.preventDefault();
		var h = $(this);
		var modal = h.closest('.modal');
		var userId = h.data('user-id');
		var userName = h.data('user-name');
		var usergroup = h.closest('.usergroup');
		var btnUser = $('.btn-value', usergroup);
		$('[name="ML_user"]', modal).val(userId);
		btnUser.html(userName + '<span class="caret"></span>');
		getCategory(modal, function(){});
	});

	$(mainModal).on('click', '.submitAdd', function(e){
		e.preventDefault();
		var h = $(this);
		var modal = h.closest('.modal');
		var data = getValueForm(modal);
		h.css({cursor: 'wait'});
		var url = '/ajax/transaction/add';
		submitForm(url, data, function(result){
			activeForm = true;
			h.css({cursor: 'pointer'});
			if(result == 'Add OK') window.location.reload();
			else {
				var errorAlert = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove-sign"></i></button>'+ result +'</div>';
				$('#myModal .modal-body').prepend(errorAlert);
				setTimeout(function(){$('.alert').slideUp()}, 2000);
			}
		});
	});

	$(document).on('click', '.submitEdit', function(e){
		e.preventDefault();
		var h = $(this);
		var modal = h.closest('.modal');
		var data = getValueForm(modal);
		h.css({cursor: 'wait'});
		var url = '/ajax/transaction/' + transactionId + '/edit';
		console.log(data);
		submitForm(url, data, function(result){
			activeForm = true;
			h.css({cursor: 'pointer'});
			if(result == 'Edit OK') window.location.reload();
			else {
				var errorAlert = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove-sign"></i></button>'+ result +'</div>';
				$('.modal-body', modal).prepend(errorAlert);
				setTimeout(function(){$('.alert').slideUp()}, 2000);
			}
		});
	});

	$(mainModal).on('click', '.submitAddMore', function(e){
		e.preventDefault();
		addMoreActive = true;
		var h = $(this);
		h.css({cursor: 'wait'});
		var modal = h.closest('.modal');
		var data = getValueForm(modal);
		var url = '/ajax/transaction/add';
		submitForm(url, data, function(result){
			activeForm = true;
			h.css({cursor: 'pointer'});
			if(result == 'Add OK') {
				$('#myModal').html(mainForm);
				getCategory($('#myModal'), function(){
					$('[href="#expenseTab"]').click();
				});
				dateCheckOut;
			}
			else {
				var errorAlert = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove-sign"></i></button>'+ result +'</div>';
				$('#myModal .modal-body').prepend(errorAlert);
				setTimeout(function(){$('.alert').slideUp()}, 2000);
			}
		});
	});

	var getCategory = function(modal, callback){
		var user = $('[name="ML_user"]', modal).val();
		var getIncome = function(callback){
			$.get('/ajax/category/1?acc_id=' + user, function(result){
				$('#incomeTab', modal).html(result);
				callback(true);
			});
		};
		var getExpense = function(callback){
			$.get('/ajax/category/2?acc_id=' + user, function(result){
				$('#expenseTab', modal).html(result);
				callback(true);
			});
		};
		getIncome(function(){
			getExpense(function(){
				callback(true);
			});
		});
	};

	var getValueForm = function(modal){
		var ML_value = $('[name="ML_value"]', modal).val();
		var ML_date = $('[name="ML_date"]', modal).val() || moment(new Date()).format($('[name="ML_date"]', modal).data('moment-format'));
		var ML_note = $('[name="ML_more"]', modal).val();
		var ML_user = $('[name="ML_user"]', modal).val();
		var ML_type = $('[name="ML_type"]', modal).val();
		var ML_type_value = $('[name="ML_type_value"]', modal).val();
		return data = {
			amount: ML_value,
			date: ML_date,
			note: ML_note,
			account_id: ML_user,
			cate_id: ML_type_value
		};
	};

	var submitForm = function(url ,data, callback){
		if(activeForm){
			activeForm = false;
			$.post(url, data, function(result){
			 	callback(result);
			 	activeForm = true;
			 });
		}
	};
	
	$('.btn-edit').click(function(e){
		e.preventDefault();
		var h = $(this);
		var userid = h.data('userid');
		$('#editTransactionModal [name="ML_user"]').val(userid);
		transactionId = h.closest('.item-transactions').data('transaction');
		getUserEdit(transactionId);
	});
	var getUserEdit = function(transactionId){
		getCategory($('#editTransactionModal'), function(){
			$.get('/ajax/transaction/' + transactionId ,function(data){
				parseData(data);
			},'JSON');
		});
	};
	var parseData = function(data){
		var userId = data.account.userId;
		var userName = data.account.name;
		var currency = data.account.currencyItem.curSymbol;
		var amount = data.amount;
		var date = data.date.date;
		var note = data.note;
		var categoryId = data.category.id;
		var categoryType = data.category.type;
		var modal = $('#editTransactionModal');
		$('[data-form="currency"]', modal).text(currency);
		$('[name="ML_value"]', modal).val(amount);
		$('[name="ML_date"]', modal).val(moment(date).format($('[name="ML_date"]', modal).data('moment-format')));
		$('[name="ML_note"]', modal).val(note);
		$('.btn-value', modal).text(userName);
		$('[data-value="ML_user"]', modal).val(userId);
		$('[name="ML_type"]').val(categoryType);
		$('[name="ML_type_value"]').val(categoryId);
		console.log(categoryId + '_' + categoryType + '_' + typeof categoryType);
		if(categoryType == 1){
			$('[href="#incomeTab"]', modal).click();
		} else {
			$('[href="#expenseTab"]', modal).click();
		}
		$('li[data-value="'+ categoryId +'"]', modal).addClass('active');
		modal.modal('show');
	};
});
